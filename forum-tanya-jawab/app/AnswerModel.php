<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerModel extends Model
{
    protected $table = "answers";
    protected $fillable = ["id","users_id","questions_id","jawaban",'gambar'];

    public function questions() {
        return $this->belongsTo('App\QuestionsModel','questions_id');
    }

    
    public function users() {
        return $this->belongsTo('App\AnggotaModel','users_id');
    }
}
