<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaModel extends Model
{
    protected $table ="users";
    protected $primaryKey ="id";
    protected $fillable = ["id","nama","email","password"];

    public function profil() {
        return $this->hasOne('App\ProfileModel','users_id');
    }
    public function question()
    {
        return $this->hasMany('App\QuestionsModel');
    }
}
