<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionsModel extends Model
{
    protected $table = "questions";
    protected $fillable = ["id", "users_id", "kategori_id", "judul", "isi", "gambar"];

    public function kategori() {
        return $this->belongsTo('App\KategoriModel','kategori_id');
    }

    public function anggota() {
        return $this->belongsTo('App\AnggotaModel','users_id');
    }

    public function answers() {
        return $this->hasMany('App\AnswerModel');
    }
    public function user() {
        return $this->belongsTo('App\UserModel','users_id');
    }
}
