<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionsModel;
use App\KategoriModel;
use App\UserModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $questions = QuestionsModel::with('user','kategori')->get();
        $kategori=KategoriModel::all();
        // return($questions);
        return view('home',compact('kategori','questions','kategori'));
    }
    public function home_login()
    {
        $questions = QuestionsModel::with('user','kategori')->get();
        $kategori=KategoriModel::all();
        // return($questions);
        return view('home_login',compact('kategori','questions','kategori'));
    }
    public function form_diskusi()
    {
        $questions = QuestionsModel::with('user','kategori')->get();
        $kategori=KategoriModel::all();
        // return($questions);
        return view('form_diskusi',compact('kategori','questions','kategori'));
    }
    public function add_diskusi(Request $request){
        // dd($request->all());
        $request->validate([
            'kategori' => 'required',
            'judul' => 'required',
            'isi' => 'required',
            //'attachment' => 'required|mimes:jpeg,jpg,png|max:2200',
        ],
        [
            'kategori.required' => 'Kolom kategori harus diisi',
            'judul.required' => 'Kolom judul harus diisi',
            'isi.required' => 'Kolom isi harus diisi',
            //'attachment.required' => 'Kolom attachment harus diisi',
        ]);
        $gambar = $request->attachment;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        $komentar = new QuestionsModel;
        $komentar->users_id = 1;
        $komentar->kategori_id = $request->kategori;
        $komentar->judul = $request->judul;
        $komentar->isi = $request->isi;
        $komentar->gambar = $new_gambar;
        $komentar->save();

        $gambar->move('gambar/',$new_gambar);
        return redirect('/home_login');
    }
}
