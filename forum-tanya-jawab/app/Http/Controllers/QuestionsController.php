<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionsModel;
use App\KategoriModel;
use App\AnggotaModel;
use App\AnswerModel;

class QuestionsController extends Controller
{
    public function index() {
        $questions = QuestionsModel::all();
        //$answer = AnswerModel::where();
        return view('questions/questions', compact('questions'));
    }

    public function detail($id) {
        $questions = QuestionsModel::find($id);
        $id_question = $questions['id'];
        $answers = AnswerModel::where('questions_id', $id_question)->get();

        return view('questions/detailQuestions', ['questions' => $questions, 'answers' => $answers]);
    }
}
