<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AnggotaModel;
use App\ProfileModel;

class AnggotaController extends Controller
{
    public function index() {

        $anggota = AnggotaModel::all();
        return view('anggota/anggota', compact('anggota'));
    }

    public function detail($id) {

        $anggota = AnggotaModel::find($id);
        return view('anggota/detailAnggota', compact('anggota'));
    }

    public function edit($id) {

        $anggota = AnggotaModel::find($id);
        return view('anggota/editAnggota', compact('anggota'));
    }

    public function update($id, Request $request) {
        $request->validate([
    		'nama' => 'required',
    		'umur' => 'required',
    		'bio' => 'required',
    		'alamat' => 'required',
    		'email' => 'required'
    	],
        [
            'nama.required' => 'Kolom nama harus diisi',
            'umur.required' => 'Kolom nama harus diisi',
            'bio.required' => 'Kolom nama harus diisi',
            'alamat.required' => 'Kolom nama harus diisi',
            'email.required' => 'Kolom nama harus diisi'
        ]);
        
        $anggota = AnggotaModel::find($id);
        $anggota->nama = $request->nama;
        $anggota->email = $request->email;
        $anggota->update();

        $profil_id = $request->profil_id;
        $profil= ProfileModel::find($profil_id);
        $profil->umur = $request->umur;
        $profil->bio = $request->bio;
        $profil->alamat = $request->alamat;
        $profil->update();

        return redirect('/anggota');

    }

    public function hapus($id, Request $request) {
        $anggota = AnggotaModel::find($id);
        $profil_id = $request->profil_id;
        $profil = ProfileModel::find($id);

        $anggota->delete();
        $profil->delete();

        return redirect('/anggota');
    }

    public function profil_admin() {
        $profil_admin = AnggotaModel::where('level', 'ADMIN')->get();
        return view('anggota/profil_admin', compact('profil_admin'));
    }

}
