<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriModel;

class KategoriController extends Controller
{
    public function index() {
        $kategori=KategoriModel::all();
        return view('kategori/Kategori',compact('kategori'));
    }

    public function create() {
        $notification = array(
            'message' => 'Post created successfully!',
            'alert-type' => 'success'
        );
        return view ('kategori/CreateKategori')->with($notification);
    }

    public function simpan(Request $request){

        $this->validate($request,[
    		'nama' => 'required'
    	],
        [
            'nama.required' => 'Kolom nama harus diisi'
        ]);

        KategoriModel::create([
    		'id' => null,
    		'nama' => $request->nama
    	]);

        return redirect('/kategori');
    }

    public function edit($id) {
        $kategori = KategoriModel::find($id);
        return view('kategori/editKategori', compact('kategori'));
    }

    public function update($id, Request $request) {

        $request->validate([
    		'nama' => 'required'
    	],
        [
            'nama.required' => 'Kolom nama harus diisi'
        ]);
        
        $kategori = KategoriModel::find($id);
        $kategori->nama = $request->nama;
        $kategori->update();
        return redirect('/kategori');

    }

    public function hapus($id) {
        $kategori = KategoriModel::find($id);
        $kategori->delete();
        return redirect('/kategori');
    }

}