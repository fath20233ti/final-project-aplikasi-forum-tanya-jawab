<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AnswerModel;

class AnswerController extends Controller
{
	public function create(Request $request){
		// dd($request->all());
    	$this->validate($request,[
    		'message' => 'required'
    	],
        [
            'message.required' => 'Kolom message harus diisi'
        ]);

        AnswerModel::create([
    		'id' => null,
    		'users_id' => $request->users_id,
    		'questions_id' => $request->questions_id,
    		'jawaban' => $request->message,
    		'gambar' => null,
    	]);
    	// print_r($request->questions_id);

        return redirect('/home_login');
    }
}
