<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileModel extends Model
{
    protected $table ="profil";
    protected $fillable = ["id","umur","bio","alamat"];

    public function anggota(){
        return $this->belongsTo('App\AnggotaModel');
    }
}
