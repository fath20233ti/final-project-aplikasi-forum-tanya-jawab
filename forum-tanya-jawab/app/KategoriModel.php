<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    protected $table = "kategori";
    protected $fillable = ["id","nama"];

    public function questions()
    {
        return $this->hasMany('App\QuestionsModel');
    }

}
