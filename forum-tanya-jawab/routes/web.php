<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* --- AREA HOME --- */
Route::get('/home', 'HomeController@index');
Route::get('/home_login', 'HomeController@home_login');
Route::get('/form_diskusi', 'HomeController@form_diskusi');
Route::post('/add_questions', 'HomeController@add_diskusi');
Route::post('/add_komentar', 'AnswerController@create');
/* --- AREA SELESAI --- */

/* --- AREA ADMIN --- */
Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/kategori', 'KategoriController@index');
Route::get('/createKategori', 'KategoriController@create');
Route::post('/simpanKategori', 'KategoriController@simpan');
Route::get('/kategori/{id}/edit', 'KategoriController@edit');
Route::put('/updateKategori/{id}', 'KategoriController@update');
Route::delete('/hapusKategori/{id}', 'KategoriController@hapus');

Route::get('/questions', 'QuestionsController@index');
Route::get('/detailQuestions/{id}', 'QuestionsController@detail');
Route::get('/editAnswer/{id}', 'AnswerController@edit');
Route::get('/hapusAnswer/{id}', 'AnswerController@hapus');

Route::get('/profil_admin', 'AnggotaController@profil_admin');
Route::get('/anggota', 'AnggotaController@index');
Route::get('/detailAnggota/{id}', 'AnggotaController@detail');
Route::get('/editAnggota/{id}', 'AnggotaController@edit');
Route::put('/updateAnggota/{id}', 'AnggotaController@update');
Route::delete('/hapusAnggota/{id}', 'AnggotaController@hapus');

/* --- AREA ADMIN SELESAI --- */

Auth::routes();