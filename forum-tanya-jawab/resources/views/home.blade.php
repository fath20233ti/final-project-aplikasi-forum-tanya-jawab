@extends('layout.master_home')
@section('content')
@foreach ($questions as $value)
<div class="card card-widget">
  <div class="card-header">
    <div class="user-block">
      <img class="img-circle" src="{{asset('layout/dist/img/user1-128x128.jpg')}}" alt="User Image">
      <span class="username"><a href="#">{{$value->user['name']}}</a></span>
      <span class="description">{{$value->kategori['nama']}} - {{$value->created_at}}</span>
    </div>
    <!-- /.card-tools -->
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <img class="img-fluid pad" src="{{asset('gambar/'. $value->gambar)}}" alt="Photo">

    <p>{{$value->isi}}</p>
    <span class="float-right text-muted">3 comments</span>
  </div>
  <!-- /.card-body -->
  <div class="card-footer card-comments">
    <?php $answer = komentar($value->id); ?>
    @foreach ($answer as $answer)
    <div class="card-comment">
      <!-- User image -->
      <img class="img-circle img-sm" src="{{asset('layout/dist/img/user3-128x128.jpg')}}" alt="User Image">

      <div class="comment-text">
        <span class="username">
          {{$answer->nama}}
          <span class="text-muted float-right">{{$answer->created_at}}</span>
        </span><!-- /.username -->
        {{$answer->jawaban}}
      </div>
      <!-- /.comment-text -->
    </div>
    @endforeach
  </div>
  <!-- /.card-footer -->
  <div class="card-footer">
    <form action="#" method="post">
      <div class="input-group">
        <input type="text" name="message" placeholder="Type Message ..." class="form-control" readonly>
        <span class="input-group-append">
          <button type="submit" class="btn btn-primary">Send</button>
        </span>
      </div>
    </form>
  </div>
  <!-- /.card-footer -->
</div>
@endforeach
@endsection