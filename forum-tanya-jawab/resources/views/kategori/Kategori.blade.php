@extends('layout.master_admin')
@section('judul')
Data Kategori
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Kategori Forum</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a href="/createKategori" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Kategori </a><br><br>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama Kategori</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    @forelse ($kategori as $key => $k)
                  <tr>
                      <td>{{$key + 1}}</td>
                      <td>{{$k->nama}}</td>
                      <td><a href="/kategori/{{$k->id}}/edit" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a>
                            <form action="/hapusKategori/{{$k->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger my-1"><i class='fa fa-trash'></i> Hapus</button>
                            </form>
                      </td>
                  </tr>
                    @empty
                  <tr>
                    <td colspan="3"><center>Belum Ada Data Kategori</td>
                  </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection

