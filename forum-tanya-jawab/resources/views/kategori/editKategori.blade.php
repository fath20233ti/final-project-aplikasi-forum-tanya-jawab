@extends('layout.master_admin')
@section('judul')
Tambah Data Kategori
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Kategori {{$kategori->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/updateKategori/{{$kategori->id}}">
                  @csrf
                  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Kategori</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="nama" value="{{$kategori->nama}}">
                  </div>
                  @error('nama')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection