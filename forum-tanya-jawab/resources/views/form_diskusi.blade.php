@extends('layout.master_homelogin')
@section('content')
<div class="card card-primary card-outline">
<div class="card-header">
  <h3 class="card-title">Form Diskusi</h3>
</div>
<!-- /.card-header -->
<form method="post" action="/add_questions" enctype="multipart/form-data">
  @csrf
<div class="card-body">
  <div class="form-group">
    <select class="form-control" name="kategori" id="kategori">
      <option disabled selected>Pilih Kategori</option>
      @foreach ($kategori as $value)
      <option value="{{$value->id}}">{{$value->nama}}</option>
      @endforeach
    </select>
  </div>
  @error('kategori')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <input class="form-control" name="judul" placeholder="Judul">
  </div>
  @error('judul')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
      <textarea name="isi" id="compose-textarea" class="form-control" style="height: 50%;">
        
      </textarea>
  </div>
  <input type="hidden" name="user_id" value="1">
  @error('isi')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <div class="btn btn-default">
      <i class="fas fa-paperclip"></i> Attachment
      <input type="file" name="attachment">
    </div>
    <p class="help-block">Max. 32MB</p>
  </div>
</div>
@error('attachment')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
<!-- /.card-body -->
<div class="card-footer">
  <div class="float-right">
    <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
  </div>
</div>
</form>
<!-- /.card-footer -->
@endsection