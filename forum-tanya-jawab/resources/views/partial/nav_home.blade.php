<nav class="main-header navbar navbar-expand-md navbar-dark navbar-info">
    <div class="container">
      <a href="{{asset('layout/index3.html')}}" class="navbar-brand">
        <span class="brand-text font-weight-light"><STRONG>DiskusiJCC</STRONG></span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-user"></i>
            Masuk
          </a>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" href="#">
            <i class="fa fa-lock" aria-hidden="true"></i>
            Login
          </a>
        </li>
      </ul>
    </div>
  </nav>