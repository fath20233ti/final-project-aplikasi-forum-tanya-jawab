<div class="callout callout-danger">
  <h5>Diskusi Terbaru</h5>
</div>
<div class="card">
  <div class="card-body">
  	<ul class="products-list product-list-in-card pl-2 pr-2">
      @foreach ($questions as $value)
      <li class="item">
        <div class="product-img">
          <img src="{{asset('layout/dist/img/default-150x150.png')}}" alt="Product Image" class="img-size-50">
        </div>
        <div class="product-info">
          <a href="javascript:void(0)" class="product-title">{{$value->user['name']}}</a>
          <span class="product-description">
            {{$value->judul}}
          </span>
        </div>
      </li>
      @endforeach
  	</ul>
  </div>
</div>

<!-- kategori -->
<div class="callout callout-danger">
  <h5>Kategori</h5>
</div>
<div class="card">
  <div class="card-body">
    <ul class="nav nav-pills flex-column">
      @foreach ($kategori as $value)
      <li class="nav-item active">
        <a href="#" class="nav-link">
          <i class="fas fa-inbox"></i> {{$value->nama}}
        </a>
      </li>
      @endforeach
    </ul>
  </div>
</div>