@extends('layout.master_admin')
@section('judul')
Tambah Data Kategori
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Detail Anggota {{$anggota->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/updateKategori/{{$anggota->id}}">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="nama" value="{{$anggota->nama}}" readonly>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="nama" value="{{$anggota->profil->umur}}" readonly>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Bio</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" rows="5" readonly>{{$anggota->profil->bio}}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" rows="5" readonly>{{$anggota->profil->alamat}}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" cols="10" readonly>{{$anggota->email}}</textarea>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection