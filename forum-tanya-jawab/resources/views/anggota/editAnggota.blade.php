@extends('layout.master_admin')
@section('judul')
Edit Data Anggota
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Anggota {{$anggota->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/updateAnggota/{{$anggota->id}}">
                  @csrf
                  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="profil_id" value="{{$anggota->profil_id}}">
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="nama" value="{{$anggota->nama}}"  >
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="umur" value="{{$anggota->profil->umur}}"  >
                    @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Bio</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" rows="5"  >{{$anggota->profil->bio}}</textarea>
                    @error('bio')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="alamat" rows="5"  >{{$anggota->profil->alamat}}</textarea>
                    @error('alamat')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" value="{{$anggota->email}}"  >
                    @error('email')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection