@extends('layout.master_admin')
@section('judul')
PROFIL ADMIN
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Profil Admin</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/updateKategori/{{$profil_admin->id}}">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="nama" value="{{$profil_admin->nama}}" readonly>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori" name="nama" value="{{$profil_admin->profil->umur}}" readonly>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Bio</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" rows="5" readonly>{{$profil_admin->profil->bio}}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" rows="5" readonly>{{$profil_admin->profil->alamat}}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <textarea class="form-control" id="exampleInputEmail1" placeholder="" name="bio" cols="10" readonly>{{$profil_admin->email}}</textarea>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection