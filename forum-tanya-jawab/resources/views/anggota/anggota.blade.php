@extends('layout.master_admin')
@section('judul')
Data Anggota Forum
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Anggota Forum</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!--<a href="/createKategori" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Anggota </a><br><br>-->
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    @forelse ($anggota as $key => $a)
                  <tr>
                      <td>{{$key + 1}}</td>
                      <td>{{$a->nama}}</td>
                      <td>{{$a->profil->umur}}</td>
                      <td>{{$a->profil->bio}}</td>
                      <td>{{$a->profil->alamat}}</td>
                      <td>{{$a->email}}</td>
                      <td>
                        <a href="/detailAnggota/{{$a->id}}" class="btn btn-info"><i class="fas fa-eye"></i> Lihat</a>
                        <a href="/editAnggota/{{$a->id}}" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a>
                        <form action="/hapusAnggota/{{$a->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="profil_id" value="{{$a->profil_id}}">
                                <button type="submit" class="btn btn-danger my-1"><i class='fa fa-trash'></i> Hapus</button>
                            </form>
                      </td>
                  </tr>
                    @empty
                  <tr>
                    <td colspan="3"><center>Belum Ada Data Kategori</td>
                  </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection
=======
@endsection
