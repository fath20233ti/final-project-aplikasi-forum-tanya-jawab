@extends('layout.master_admin')
@section('judul')
Topik
@endsection

@section('content')
<div class="card card-widget">
  <div class="card-header">
    <div class="user-block">
      <img class="img-circle" src="{{asset('layout/dist/img/user1-128x128.jpg')}}" alt="User Image">
      <span class="username"><a href="#">{{$questions->anggota->nama}}</a></span>
      <span class="description">Kategori : {{$questions->kategori->nama}}</span>
    </div>
    <!-- /.card-tools -->
  </div>
  <!-- /.card-header -->
  <div class="card-body">
      <h2>{{$questions->judul}}</h2>
      @if($questions->gambar == null)

      <p>{{$questions->isi}}</p>

      @else 
     <img class="img-fluid pad" src="{{asset('layout/dist/img/photo2.png')}}" alt="Photo">
     <p>{{$questions->isi}}</p>

      @endif

    <span class="float-right text-muted">3 comments</span>
  </div>
  <!-- /.card-body -->
  <div class="card-footer card-comments">


  @foreach ($answers as $an)
  <div class="card-comment">
      <!-- User image -->
      <img class="img-circle img-sm" src="{{asset('layout/dist/img/user3-128x128.jpg')}}" alt="User Image">

        <div class="comment-text">
            <span class="username">
            {{$an->users->nama}}
            <span class="text-muted float-right"><a href="/editAnswer/{id}"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;<a href="/hapusAnswer/{id}"><i class="fa fa-trash"></i></a></span>
            </span><!-- /.username -->
            {{$an->jawaban}}
        </div>
        
      </div>
      <!-- /.comment-text -->
    <!-- /.card-comment -->
    @endforeach

  <!-- /.card-footer -->
  <div class="card-footer">
    <form action="#" method="post">
      <img class="img-fluid img-circle img-sm" src="{{asset('layout/dist/img/user4-128x128.jpg')}}" alt="Alt Text">
      <!-- .img-push is used to add margin to elements next to floating images -->
      <div class="img-push">
        <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
      </div>
    </form>
  </div>
  <!-- /.card-footer -->
</div>
@endsection