@extends('layout.master_admin')
@section('judul')
Data Kategori
@endsection

@section('content')
    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Forum Diskusi</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Judul Forum</th>
                    <th>Kategori</th>
                    <th>Penulis</th>
                    <th>Komentar</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    @forelse ($questions as $key => $f)
                  <tr>
                      <td>{{$key +1}}</td>
                      <td>{{$f->judul}}</td>
                      <td>{{$f->kategori->nama}}</td>
                      <td>{{$f->anggota->nama}}</td>
                      <td>{{$f->anggota->nama}}</td>
                      <td><a href="/detailQuestions/{{$f->id}}" class="btn btn-info"><i class="fas fa-eye"></i> Detail</a>
                            <form action="/hapusKategori/{{$f->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger my-1"><i class='fa fa-trash'></i> Hapus</button>
                            </form>
                      </td>
                  </tr>
                    @empty
                  <tr>
                    <td colspan="3"><center>Belum Ada Data Kategori</td>
                  </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
@endsection